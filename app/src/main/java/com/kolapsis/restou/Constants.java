package com.kolapsis.restou;

import com.kolapsis.restou.helper.NotificationHelper;

public interface Constants {
    boolean DEBUG = true;

    String APP_NAME = "Restou";
    String APP_PACKAGE = "com.kolapsis.restou";

    int BDD_VERSION = 7;
    String BDD_NAME = "restou.db";

    String ACCOUNT_TYPE = APP_PACKAGE + ".account";
    String AUTH_TOKEN_TYPE = APP_PACKAGE + ".token";
    String API_URL = "http://api-ril.kolapsis.com/";
    String API_TOKEN_HEADER = "X-APP-TOKEN";

    String AUTHORITY = APP_PACKAGE + ".provider";
    String CALLER_IS_SYNCADAPTER = "caller_is_syncadapter";

    String SHOW_VOTES_RESULT = APP_PACKAGE + ".Votes";

    int NOTIFICATION_CHOICE_ID = 1;
    int NOTIFICATION_DONE = 2;
    String NOTIFICATION_DONE_ACTION = APP_PACKAGE + ".action.NOTIFICATION_DONE";
    String CHECK_FOR_VOTE_OR_RESULTS_ACTIONS = APP_PACKAGE + ".action.CHECK_FOR_VOTE_OR_RESULTS_ACTIONS";

    int RESULT_ADD_ACCOUNT = 1;
    int RESTAURANT_LOADER = 100;

    int LIMIT_HOUR = 18;
}
