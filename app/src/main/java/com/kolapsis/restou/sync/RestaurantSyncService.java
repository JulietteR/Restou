package com.kolapsis.restou.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * Created by Benjamin Touchard on 18/04/17.
 * http://www.kolapsis.com
 */
public class RestaurantSyncService extends Service {

    private static final Object lock = new Object();
    private static RestaurantSyncAdapter syncAdapter = null;

    @Override
    public void onCreate() {
        synchronized (lock) {
            if (syncAdapter == null)
                syncAdapter = new RestaurantSyncAdapter(getApplicationContext(), true);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return syncAdapter.getSyncAdapterBinder();
    }
}
