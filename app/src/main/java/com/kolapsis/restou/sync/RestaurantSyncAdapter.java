package com.kolapsis.restou.sync;

import android.accounts.Account;
import android.accounts.AuthenticatorException;
import android.content.*;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import com.kolapsis.restou.Constants;
import com.kolapsis.restou.api.Api;
import com.kolapsis.restou.entity.AbstractEntity;
import com.kolapsis.restou.entity.Restaurant;
import com.kolapsis.restou.entity.Vote;
import com.kolapsis.restou.helper.AccountHelper;
import com.kolapsis.restou.helper.RestaurantHelper;
import com.kolapsis.restou.helper.VoteHelper;

import java.io.IOException;
import java.util.List;

/**
 * Created by Benjamin Touchard on 18/04/17.
 * http://www.kolapsis.com
 */
public class RestaurantSyncAdapter extends AbstractThreadedSyncAdapter {

    public static final String TAG = Constants.APP_NAME + ".RestaurantSyncAdapter";

    public RestaurantSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient contentProviderClient, SyncResult syncResult) {
        if (extras.containsKey("upload") && extras.getBoolean("upload", false)) return;
        AccountHelper.setContext(getContext());
        long lastSyncTime = AccountHelper.updateSyncState(authority, AccountHelper.SYNC_PROGRESS);
        if (Constants.DEBUG) lastSyncTime = 0;
        try {
            String token = AccountHelper.blockingGetAuthToken(account, Constants.AUTH_TOKEN_TYPE, false);
            if (!TextUtils.isEmpty(token)) {
                performSyncRestaurant(account, token);
                performSyncVote(account, token);
            }
        } catch (AuthenticatorException | IOException e) {
            Log.e(getClass().getName(), e.getMessage(), e);
        } finally {
            AccountHelper.updateSyncState(authority, AccountHelper.SYNC_FINISH);
        }
    }

    private void performSyncRestaurant(Account account, String token) {
        // Sync des suppression locales
        List<Restaurant> all = RestaurantHelper.getMarkedAs(account, AbstractEntity.MarkedAs.DELETED);
        for (Restaurant restaurant : all) {
            if (Api.getInstance().delete(restaurant, token))
                RestaurantHelper.delete(account, restaurant, true);
        }
        // Sync des insertions ou maj locales
        all = RestaurantHelper.getMarkedAs(account, AbstractEntity.MarkedAs.INSERT_OR_UPDATE);
        for (Restaurant restaurant : all)
            if (Api.getInstance().save(restaurant, token, account))
                RestaurantHelper.update(account, restaurant, true);
        // Sync des changements provenant du serveur
        List<Restaurant> locals = RestaurantHelper.select(account);
        List<Restaurant> remotes = Api.getInstance().load(Restaurant.class, token, account, 0);
        for (Restaurant remote : remotes) {
            Restaurant local = getLocalEntity(locals, remote.getSourceId());
            if (local == null) RestaurantHelper.insert(account, remote, true);
            else {
                locals.remove(local);
                remote.setId(local.getId());
                RestaurantHelper.update(account, remote, true);
            }
        }
        // Supression des remotes
        for (Restaurant restaurant : locals)
            RestaurantHelper.delete(account, restaurant);
    }

    private void performSyncVote(Account account, String token) {
        // Sync des suppression locales
        List<Vote> all = VoteHelper.getMarkedAs(account, AbstractEntity.MarkedAs.DELETED);
        for (Vote vote : all) {
            if (Api.getInstance().delete(vote, token))
                VoteHelper.delete(account, vote, true);
        }
        // Sync des insertions ou maj locales
        all = VoteHelper.getMarkedAs(account, AbstractEntity.MarkedAs.INSERT_OR_UPDATE);
        for (Vote vote : all)
            if (Api.getInstance().save(vote, token, account))
                VoteHelper.update(account, vote, true);
        // Sync des changements provenant du serveur
        List<Vote> locals = VoteHelper.select(account);
        List<Vote> remotes = Api.getInstance().load(Vote.class, token, account, 0);
        for (Vote remote : remotes) {
            Vote local = getLocalEntity(locals, remote.getSourceId());
            if (local == null) VoteHelper.insert(account, remote, true);
            else {
                locals.remove(local);
                remote.setId(local.getId());
                VoteHelper.update(account, remote, true);
            }
        }
        // Supression des remotes
        for (Vote vote : locals)
            VoteHelper.delete(account, vote, true);
    }

    private <T extends AbstractEntity> T getLocalEntity(List<T> list, int sourceId) {
        for (T entity : list)
            if (sourceId == entity.getSourceId())
                return entity;
        return null;
    }
}
