package com.kolapsis.restou.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import com.kolapsis.restou.Constants;
import com.kolapsis.restou.entity.Restaurant;
import com.kolapsis.restou.entity.Vote;
import com.kolapsis.restou.helper.DatabaseHelper;

/**
 * Created by Benjamin Touchard on 18/04/17.
 * http://www.kolapsis.com
 */
public class RestaurantProvider extends ContentProvider {

    private static final String TAG = Constants.APP_NAME + ".RestaurantProvider";
    public static final String AUTHORITY = Constants.AUTHORITY + ".Restaurant";

    private static final UriMatcher matcher;
    static {
        matcher = new UriMatcher(UriMatcher.NO_MATCH);

        matcher.addURI(AUTHORITY, Restaurant.TABLE, Restaurant.DIRECTORY);
        matcher.addURI(AUTHORITY, Restaurant.TABLE + "/#", Restaurant.ITEM);

        matcher.addURI(AUTHORITY, Vote.TABLE, Vote.DIRECTORY);
        matcher.addURI(AUTHORITY, Vote.TABLE + "/#", Vote.ITEM);
    }

    private DatabaseHelper sql;

    @Override
    public boolean onCreate() {
        sql = new DatabaseHelper(getContext());
        return true;
    }

    private static boolean hasCallerIsSyncAdapterParameter(Uri uri) {
        return Boolean.parseBoolean(uri.getQueryParameter(Constants.CALLER_IS_SYNCADAPTER));
    }

    @Override
    public String getType(Uri uri) {
        switch (matcher.match(uri)) {
            case Restaurant.DIRECTORY:
            case Restaurant.ITEM:
                return Restaurant.CONTENT_TYPE;
            case Vote.DIRECTORY:
            case Vote.ITEM:
                return Vote.CONTENT_TYPE;
        }
        return null;
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String where, String[] whereArgs, String order) {
        if (isItemUri(uri)) where = (where != null ? where + " AND" : "") + " _id="+uri.getLastPathSegment();
        if (projection == null) projection = getProjection(uri);
        SQLiteDatabase db = sql.getReadableDatabase();
        Cursor cursor = db.query(getTable(uri), projection, where, whereArgs, null, null, order);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public Uri insert(@NonNull Uri uri, @NonNull ContentValues contentValues) {
        if (!isTableUri(uri)) throw new IllegalArgumentException("Not URI table for: " + uri);
        SQLiteDatabase db = sql.getWritableDatabase();
        long rowId = db.insert(getTable(uri), null, contentValues);
        if (rowId > 0) {
            boolean syncToNetwork = !hasCallerIsSyncAdapterParameter(uri);
            Uri newUri = ContentUris.withAppendedId(Restaurant.CONTENT_URI, rowId);
            getContext().getContentResolver().notifyChange(newUri, null, syncToNetwork);
            // Log.v(TAG, "inserted: " + newUri);
            return newUri;
        }
        throw new SQLiteException("Failed to insert row into " + uri);
    }

    @Override
    public int update(@NonNull Uri uri, @NonNull ContentValues contentValues, @NonNull String where, @NonNull String[] whereArgs) {
        SQLiteDatabase db = sql.getWritableDatabase();
        if (!isTableUri(uri) && !isItemUri(uri)) throw new IllegalArgumentException("Unknown URI " + uri);
        if (isItemUri(uri)) where = "_id = " + uri.getLastPathSegment();
        int count = db.update(getTable(uri), contentValues, where, whereArgs);
        boolean syncToNetwork = !hasCallerIsSyncAdapterParameter(uri);
        getContext().getContentResolver().notifyChange(uri, null, syncToNetwork);
        // Log.v(TAG, "updated: " + uri);
        return count;
    }

    @Override
    public int delete(@NonNull Uri uri, @NonNull String where, @NonNull String[] whereArgs) {
        if (!isTableUri(uri) && !isItemUri(uri)) throw new IllegalArgumentException("Unknown URI " + uri);
        if (isItemUri(uri)) where = "_id = " + uri.getLastPathSegment();
        SQLiteDatabase db = sql.getWritableDatabase();
        int count = db.delete(getTable(uri), where, whereArgs);
        boolean syncToNetwork = !hasCallerIsSyncAdapterParameter(uri);
        getContext().getContentResolver().notifyChange(uri, null, syncToNetwork);
        // Log.v(TAG, "deleted: " + uri);
        // Log.v(TAG, "deleted: " + where + " " + whereArgs[0]);
        return count;
    }

    private boolean isTableUri(@NonNull Uri uri) {
        switch (matcher.match(uri)) {
            case Restaurant.DIRECTORY:
            case Vote.DIRECTORY:
                return true;
        }
        return false;
    }

    private boolean isItemUri(@NonNull Uri uri) {
        switch (matcher.match(uri)) {
            case Restaurant.ITEM:
            case Vote.ITEM:
                return true;
        }
        return false;
    }

    private String getTable(Uri uri) {
        switch (matcher.match(uri)) {
            case Restaurant.DIRECTORY:
            case Restaurant.ITEM:
                return Restaurant.TABLE;
            case Vote.DIRECTORY:
            case Vote.ITEM:
                return Vote.TABLE;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }


    private String[] getProjection(Uri uri) {
        switch (matcher.match(uri)) {
            case Restaurant.DIRECTORY:
            case Restaurant.ITEM:
                return Restaurant.Columns.FULL_PROJECTION;
            case Vote.DIRECTORY:
            case Vote.ITEM:
                return Vote.Columns.FULL_PROJECTION;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }

}
