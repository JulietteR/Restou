package com.kolapsis.restou.entity;

import android.accounts.Account;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import com.kolapsis.restou.provider.RestaurantProvider;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Benjamin Touchard on 13/04/17.
 * http://www.kolapsis.com
 */

public class Restaurant extends AbstractEntity {

    public static final String TABLE = "restaurant";
    public static final Uri CONTENT_URI = Uri.parse("content://" + RestaurantProvider.AUTHORITY + "/" + TABLE);
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.kolapsis.restou." + TABLE;
    public static final String MIME_TYPE = "vnd.android.cursor.item/vnd.kolapsis.restou." + TABLE;
    public static final int DIRECTORY = 1;
    public static final int ITEM = 2;

    public static class Columns extends BaseColumns {

        public static final String NAME = "name";
        public static final String ADDRESS = "address";

        public static final String[] FULL_PROJECTION = new String[] {
            ID, SOURCE_ID, ACCOUNT, NAME, ADDRESS, DIRTY, DELETE
        };

        public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS "+TABLE+" (" +
                ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                SOURCE_ID + " INTEGER NOT NULL DEFAULT 0, " +
                ACCOUNT + " TEXT NOT NULL, " +
                NAME + " TEXT NOT NULL, " +
                ADDRESS + " TEXT NOT NULL, " +
                DIRTY + " INTEGER NOT NULL DEFAULT 0, " +
                DELETE + " INTEGER NOT NULL DEFAULT 0" +
                ");";

        public static final String DROP_TABLE = "DROP TABLE IF EXISTS "+TABLE+";";

    }

    private String name, address;


    public Restaurant(){}
    public Restaurant(@NonNull Cursor cursor){
        from(cursor);
    }

    @Override
    public Uri getUri() {
        return ContentUris.withAppendedId(CONTENT_URI, getId());
    }

    @Override
    public String getApiPath() {
        return "restaurant";
    }

    public String getName() {
        return name;
    }
    public Restaurant setName(@NonNull String name) {
        this.name = name;
        return this;
    }



    public String getAddress() {
        return address;
    }
    public Restaurant setAddress(@NonNull String address) {
        this.address = address;
        return this;
    }

    @Override
    public void from(@NonNull Account account, @NonNull JSONObject json) {
        try {
            if (json.has("id")) setSourceId(json.getInt("id"));
            if (json.has("name")) setName(json.getString("name"));
            if (json.has("address")) setAddress(json.getString("address"));
        } catch (JSONException e) {
            Log.e(getClass().getName(), e.getMessage(), e);
        }
    }

    @Override
    public JSONObject asJSON(@NonNull Account account) {
        try {
            JSONObject json = new JSONObject();
            json.put("id", getSourceId());
            json.put("name", getName());
            json.put("address", getAddress());
            return json;
        } catch (JSONException e) {
            Log.e(getClass().getName(), e.getMessage(), e);
        }
        return null;
    }

    @Override
    public void from(@NonNull Cursor cursor) {
        setId(cursor.getLong(cursor.getColumnIndex(Columns.ID)));
        setSourceId(cursor.getInt(cursor.getColumnIndex(Columns.SOURCE_ID)));
        setName(cursor.getString(cursor.getColumnIndex(Columns.NAME)));
        setAddress(cursor.getString(cursor.getColumnIndex(Columns.ADDRESS)));
    }

    @Override
    public ContentValues asContent(@NonNull Account account) {
        ContentValues values = new ContentValues();
        values.put(Columns.SOURCE_ID, getSourceId());
        values.put(Columns.ACCOUNT, account.name);
        values.put(Columns.NAME, getName());
        values.put(Columns.ADDRESS, getAddress());
        return values;
    }

    @Override
    public String toString() {
        return "Restaurant [" + getId() + "] " + getName();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Restaurant)) return false;
        return ((Restaurant)obj).getId() == getId();
    }
}
