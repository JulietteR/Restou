package com.kolapsis.restou.entity;

import android.accounts.Account;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Benjamin Touchard on 13/04/17.
 * http://www.kolapsis.com
 */
public abstract class AbstractEntity {

    public static class BaseColumns {

        public static final String ID = "_id";
        public static final String SOURCE_ID = "_source_id";
        public static final String ACCOUNT = "_account";
        public static final String DATA = "_data";
        public static final String DISPLAY_NAME = "_display_name";
        public static final String SIZE = "_size";
        public static final String DIRTY = "_dirty";
        public static final String DELETE = "_delete";

        public static String getWhere(@NonNull Account account) {
            return ACCOUNT + " = '" + account.name + "'";
        }
    }

    public enum MarkedAs {
        INSERT(BaseColumns.ID + " = 0"),
        UPDATE(BaseColumns.DIRTY + " = 1"),
        DELETED(BaseColumns.DELETE + " = 1"),
        INSERT_OR_UPDATE(BaseColumns.ID + " = 0 OR " + BaseColumns.DIRTY + " = 1");
        private final String value;
        MarkedAs(String value){ this.value = value; }
        public String value(){ return value; }
    }

    protected long id;
    protected int sourceId;

    public abstract String getApiPath();

    public long getId() {
        return id;
    }
    public AbstractEntity setId(long id) {
        this.id = id;
        return this;
    }

    public int getSourceId() {
        return sourceId;
    }
    public AbstractEntity setSourceId(int id) {
        this.sourceId= id;
        return this;
    }

    public abstract Uri getUri();
    public abstract void from(@NonNull Account account, @NonNull JSONObject json);
    public abstract void from(Cursor cursor);
    public abstract JSONObject asJSON(@NonNull Account account);
    public abstract ContentValues asContent(@NonNull Account account);

}
