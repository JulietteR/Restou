package com.kolapsis.restou.entity;

import android.accounts.Account;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import com.kolapsis.restou.helper.RestaurantHelper;
import com.kolapsis.restou.provider.RestaurantProvider;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Benjamin Touchard on 02/06/17.
 * http://www.kolapsis.com
 */
public class Vote extends AbstractEntity {

    public static final String TABLE = "vote";
    public static final Uri CONTENT_URI = Uri.parse("content://" + RestaurantProvider.AUTHORITY + "/" + TABLE);
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.kolapsis.restou." + TABLE;
    public static final String MIME_TYPE = "vnd.android.cursor.item/vnd.kolapsis.restou." + TABLE;
    public static final int DIRECTORY = 3;
    public static final int ITEM = 4;

    public int getUserId() {
        return userId;
    }

    public Vote setUserId(int userId) {
        this.userId = userId;
        return this;
    }

    public static class Columns extends BaseColumns {

        public static final String USER_ID = "id_user";
        public static final String RESTAURANT_ID = "id_restaurant";
        public static final String DATE = "date";

        public static final String[] FULL_PROJECTION = new String[] {
                ID, SOURCE_ID, ACCOUNT, USER_ID, RESTAURANT_ID, DATE, DIRTY, DELETE
        };

        public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS "+TABLE+" (" +
                ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                SOURCE_ID + " INTEGER NOT NULL DEFAULT 0, " +
                ACCOUNT + " TEXT NOT NULL, " +
                USER_ID + " INTEGER NOT NULL, " +
                RESTAURANT_ID + " INTEGER NOT NULL, " +
                DATE + " TEXT NOT NULL, " +
                DIRTY + " INTEGER NOT NULL DEFAULT 0, " +
                DELETE + " INTEGER NOT NULL DEFAULT 0" +
                ");";

        public static final String DROP_TABLE = "DROP TABLE IF EXISTS "+TABLE+";";

    }

    private int userId;
    private long restaurant;
    private Date date;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.FRANCE);

    public Vote(){}
    public Vote(@NonNull Cursor cursor){
        from(cursor);
    }

    @Override
    public Uri getUri() {
        return ContentUris.withAppendedId(CONTENT_URI, getId());
    }

    @Override
    public String getApiPath() {
        return "vote";
    }

    public long getRestaurant() {
        return restaurant;
    }
    public Vote setRestaurant(long id) {
        restaurant = id;
        return this;
    }

    public Date getDate() {
        return date;
    }
    public Vote setDate(String value) {
        try {
            setDate(sdf.parse(value));
        } catch (ParseException ignored) {}
        return this;
    }
    public Vote setDate(@NonNull Date date) {
        this.date = date;
        return this;
    }

    @Override
    public void from(@NonNull Account account, @NonNull JSONObject json) {
        try {
            if (json.has("id")) setSourceId(json.getInt("id"));
            if (json.has("id_user")) setSourceId(json.getInt("id_user"));
            if (json.has("id_restaurant")) setRestaurant(RestaurantHelper.getIdFromSourceId(account, json.getInt("id_restaurant")));
            if (json.has("date")) setDate(sdf.parse(json.getString("date")));
        } catch (JSONException | ParseException e) {
            Log.e(getClass().getName(), e.getMessage(), e);
        }
    }

    @Override
    public JSONObject asJSON(@NonNull Account account) {
        try {
            JSONObject json = new JSONObject();
            json.put("id", getSourceId());
            json.put("id_user", "?");
            json.put("id_restaurant", RestaurantHelper.getSourceIdFromId(account, restaurant));
            json.put("date", "?");
            return json;
        } catch (JSONException e) {
            Log.e(getClass().getName(), e.getMessage(), e);
        }
        return null;
    }

    @Override
    public void from(@NonNull Cursor cursor) {
        setId(cursor.getLong(cursor.getColumnIndex(Columns.ID)));
        setSourceId(cursor.getInt(cursor.getColumnIndex(Columns.SOURCE_ID)));
        setUserId(cursor.getInt(cursor.getColumnIndex(Columns.USER_ID)));
        setRestaurant(cursor.getLong(cursor.getColumnIndex(Columns.RESTAURANT_ID)));
        setDate(cursor.getString(cursor.getColumnIndex(Columns.DATE)));
    }

    @Override
    public ContentValues asContent(@NonNull Account account) {
        ContentValues values = new ContentValues();
        values.put(Columns.SOURCE_ID, getSourceId());
        values.put(Columns.ACCOUNT, account.name);
        values.put(Columns.RESTAURANT_ID, restaurant);
        values.put(Columns.USER_ID, getId() );
        values.put(Columns.DATE, sdf.format(getDate()));
        return values;
    }

    @Override
    public String toString() {
        return "Vote [" + id + "], sourceId: " + sourceId + ", restaurant: " + restaurant + " " + date;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Vote)) return false;
        return ((Vote)obj).getId() == getId();
    }
}
