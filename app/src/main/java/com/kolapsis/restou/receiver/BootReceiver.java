package com.kolapsis.restou.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.kolapsis.restou.Constants;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Benjamin Touchard on 02/06/17.
 * http://www.kolapsis.com
 */
public class BootReceiver extends BroadcastReceiver {

    private AlarmManager alarmMgr;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

            Calendar calendar = Calendar.getInstance(Locale.FRANCE);
            calendar.setTimeInMillis(System.currentTimeMillis());
            calendar.set(Calendar.HOUR_OF_DAY, Constants.LIMIT_HOUR);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);

            Intent alarm = new Intent(Constants.CHECK_FOR_VOTE_OR_RESULTS_ACTIONS);
// alarm.setAction(Constants.SHOW_VOTES_RESULT);
            PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, alarm, 0);
//check si l'utilisateur ou non a voté sinon lui demander de vote pour accéder aux résultatsw
            alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                    AlarmManager.INTERVAL_DAY, alarmIntent);
        }
    }
}
