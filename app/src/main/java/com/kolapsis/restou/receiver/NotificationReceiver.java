package com.kolapsis.restou.receiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.kolapsis.restou.Constants;
import com.kolapsis.restou.helper.NotificationHelper;

/**
 * Created by Juliette RIVIERE on 27/06/2017.
 */
public class NotificationReceiver extends BroadcastReceiver{
    public NotificationReceiver() {

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if(Constants.NOTIFICATION_DONE_ACTION.equals(intent.getAction())) {
            NotificationHelper.close(context);
        }
    }
}
