package com.kolapsis.restou.receiver;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import com.kolapsis.restou.Constants;
import com.kolapsis.restou.R;

/**
 * Created by Benjamin Touchard on 02/06/17.
 * http://www.kolapsis.com
 */
public class VoteNotification extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (Constants.SHOW_VOTES_RESULT.equals(intent.getAction())) {
            NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Résultat des votes")
                        .setContentText("Et voilà on mange à ...");
            NotificationManager mNotifyMgr =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            mNotifyMgr.notify(1, mBuilder.build());
        }
    }
}
