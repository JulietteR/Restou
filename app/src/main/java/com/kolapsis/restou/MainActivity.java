package com.kolapsis.restou;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.*;
import android.widget.Button;
import android.widget.TextView;
import com.kolapsis.restou.api.Api;
import com.kolapsis.restou.entity.Restaurant;
import com.kolapsis.restou.entity.Vote;
import com.kolapsis.restou.helper.AccountHelper;
import com.kolapsis.restou.helper.NotificationHelper;
import com.kolapsis.restou.helper.RestaurantHelper;
import com.kolapsis.restou.helper.VoteHelper;
import com.kolapsis.restou.provider.RestaurantProvider;

import java.io.IOException;
import java.util.Calendar;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    public interface IRestaurantClick {
        void onClick(Restaurant restaurant);
    }

    private static class VoteTask extends AsyncTask<Restaurant, Void, Boolean> {
        public interface IResult {
            void onResult(boolean success);
        }

        private Context context;
        private IResult listener;

        public VoteTask(Context context, IResult listener) {
            this.context = context;
            this.listener = listener;
        }

        @Override
        protected Boolean doInBackground(Restaurant... restaurants) {
            try {
                Account account = AccountHelper.getAccount();
                String token = AccountHelper.blockingGetAuthToken(account, Constants.AUTH_TOKEN_TYPE, false);
                if (!TextUtils.isEmpty(token)) {
                    Vote vote = new Vote();
                    vote.setRestaurant(restaurants[0].getId());
                    return Api.getInstance().save(vote, token, account);
                }
            } catch (AuthenticatorException | IOException e) {
                Log.e(TAG, e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (success) {
                Bundle extras = new Bundle();
                extras.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
                ContentResolver.requestSync(AccountHelper.getAccount(), RestaurantProvider.AUTHORITY, extras);
            } else {
                new AlertDialog.Builder(context).setTitle("Erreur").setMessage("Vous avez déjà voté aujourd'hui !!!").create().show();
            }
            if (listener != null) {
                listener.onResult(success);
            }
        }
    }

    private static final String TAG = "MainActivity";

    private boolean canVote;
    private RecyclerView recycler;
    private RestaurantAdapter adapter;
    private IRestaurantClick listener = new IRestaurantClick() {
        @Override
        public void onClick(final Restaurant restaurant) {
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("Vote")
                    .setMessage("Confirme-tu ton vote pour\n" + restaurant.getName())
                    .setPositiveButton("OUI", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            new VoteTask(MainActivity.this, new VoteTask.IResult() {
                                @Override
                                public void onResult(boolean success) {
                                    if (success) {
                                        adapter.setCanVote(false);
                                    }
                                }
                            }).execute(restaurant);
                        }
                    })
                    .setNegativeButton("NON", null)
                    .create().show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // DatabaseHelper.invalidate(this);
        AccountHelper.setContext(this);
        checkAccount();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getSupportLoaderManager().destroyLoader(Constants.RESTAURANT_LOADER);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sync:
                Bundle extras = new Bundle();
                extras.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
                ContentResolver.requestSync(AccountHelper.getAccount(), RestaurantProvider.AUTHORITY, extras);
            case R.id.preferences:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }

    }

    private void checkAccount() {
        if (!AccountHelper.hasAccount()) {
            showAddAccount();
        } else {
            showApp();
        }
    }

    private void showAddAccount() {
        setContentView(R.layout.activity_main_no_account);
        Button btn = (Button) findViewById(R.id.add_account);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addAccount();
            }
        });
        addAccount();
    }

    private void addAccount() {
        Intent intent = AccountManager.newChooseAccountIntent(null, null, new String[]{Constants.ACCOUNT_TYPE}, null, null, null, null);
        startActivityForResult(intent, Constants.RESULT_ADD_ACCOUNT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.RESULT_ADD_ACCOUNT && resultCode == RESULT_OK) {
            checkAccount();
        }
    }

    private void showApp() {
        setContentView(R.layout.activity_main);
        Calendar now = Calendar.getInstance(Locale.FRANCE);
        Calendar start = Calendar.getInstance(Locale.FRANCE);
        start.set(Calendar.HOUR, 0);
        start.set(Calendar.MINUTE, 0);
        start.set(Calendar.SECOND, 0);
        start.set(Calendar.MILLISECOND, 0);
        Calendar end = Calendar.getInstance(Locale.FRANCE);
        end.set(Calendar.HOUR, Constants.LIMIT_HOUR);
        end.set(Calendar.MINUTE, 0);
        end.set(Calendar.SECOND, 0);
        end.set(Calendar.MILLISECOND, 0);
        canVote = now.after(start) && now.before(end);
        int id = Integer.parseInt(AccountHelper.getData("id"));
        int count = VoteHelper.count(AccountHelper.getAccount(), id);
        Log.v(TAG, "usr count:" + count);
        initializeList(now.after(start) && now.before(end));
        findViewById(R.id.test).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NotificationHelper.showChoice(MainActivity.this);
            }
        });
        findViewById(R.id.triggerNotif).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NotificationHelper.triggerNotification(MainActivity.this);
            }
        });
    }

    private void initializeList(boolean canVote) {
        recycler = (RecyclerView) findViewById(R.id.list);
        recycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new RestaurantAdapter(this, canVote);
        adapter.setListener(listener);
        recycler.setAdapter(adapter);
        getSupportLoaderManager().initLoader(Constants.RESTAURANT_LOADER, null, this);

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new RestaurantHelper.Loader(this, AccountHelper.getAccount(), Restaurant.CONTENT_URI);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // Log.v(TAG, "onLoadFinished: " + data.getCount());
        adapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }

    public static class RestaurantAdapter extends RecyclerView.Adapter<RestaurantAdapter.ViewHolder> {

        public void setCanVote(boolean canVote) {
            this.canVote = canVote;
            notifyDataSetChanged();
        }

        public static class ViewHolder extends RecyclerView.ViewHolder {
            private TextView label, count;

            public ViewHolder(View view) {
                super(view);
                label = (TextView) view.findViewById(R.id.label);
                count = (TextView) view.findViewById(R.id.count);
            }
        }

        private boolean canVote;
        private Cursor cursor;
        private LayoutInflater inflater;
        private IRestaurantClick listener;

        public RestaurantAdapter(Context context, boolean canVote) {
            inflater = LayoutInflater.from(context);
            this.canVote = canVote;
        }

        public void setListener(IRestaurantClick l) {
            listener = l;
        }

        public void swapCursor(Cursor c) {
            cursor = c;
            notifyDataSetChanged();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = inflater.inflate(R.layout.item_list, parent, false);
            view.setClickable(true);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            cursor.moveToPosition(position);
            final Restaurant restaurant = new Restaurant(cursor);
            // Log.v(TAG, "id: " + restaurant.getId() + " " + restaurant.getSourceId());
            holder.label.setText(restaurant.getName());
            if (canVote) {
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (listener != null)
                            listener.onClick(restaurant);
                    }
                });
            } else {
                int cnt = VoteHelper.count(AccountHelper.getAccount(), restaurant.getId());
                holder.count.setText("" + cnt);
            }
        }

        @Override
        public int getItemCount() {
            return cursor == null ? 0 : cursor.getCount();
        }
    }
}
