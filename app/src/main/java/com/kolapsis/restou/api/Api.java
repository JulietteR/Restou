package com.kolapsis.restou.api;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorException;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import com.android.volley.*;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import com.kolapsis.restou.Constants;
import com.kolapsis.restou.entity.AbstractEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by Benjamin Touchard on 12/04/17.
 * http://www.kolapsis.com
 */
@SuppressLint("StaticFieldLeak")
public class Api {

    public static final String TAG = Constants.APP_NAME + ".Api";

    private static Context context;
    public static void setContext(Context ctx) {
        context = ctx;
    }

    private static Api instance;
    public static Api getInstance() {
        if (instance == null) instance = new Api();
        return instance;
    }

    public static final class ApiException extends Exception {
        public ApiException(String message) {
            super(message);
        }

        public ApiException(JSONObject json) throws JSONException {
            super(json.getJSONObject("result").getString("error"));
        }
    }

    public static class ApiRequest {
        private static RequestQueue queue;

        public static RequestQueue getQueue() {
            if (queue == null) queue = Volley.newRequestQueue(context);
            return queue;
        }

        private String token, url, tag = null;
        private int method;
        private boolean cache = false;
        private Map<String, String> data = new HashMap<>();

        public ApiRequest(@NonNull String url) {
            this(url, Request.Method.GET);
        }

        public ApiRequest(@NonNull String url, int method) {
            this(null, url, method);
        }

        public ApiRequest(@NonNull String token, @NonNull String url) {
            this(token, url, Request.Method.GET);
        }

        public ApiRequest(@NonNull String token, @NonNull String url, int method) {
            this.token = token;
            this.url = url;
            this.method = method;
        }

        @SuppressWarnings("unused")
        public ApiRequest shouldCache(boolean cache) {
            this.cache = cache;
            return this;
        }

        public ApiRequest tag(@NonNull String tag) {
            this.tag = tag;
            return this;
        }

        public ApiRequest token(@NonNull String token) {
            this.token = token;
            return this;
        }

        public ApiRequest data(@NonNull Map<String, String> data) {
            this.data.putAll(data);
            return this;
        }

        public ApiRequest data(@NonNull String name, String value) {
            data.put(name, value);
            return this;
        }

        public ApiRequest data(@NonNull JSONObject json) {
            for (Iterator<String> it = json.keys(); it.hasNext(); ) {
                String key = it.next();
                try { data.put(key, json.getString(key)); }
                catch (JSONException ignored) {}
            }
            return this;
        }

        public boolean success() throws ApiException {
            try {
                JSONObject json = execute();
                return json != null && json.has("success") && json.getBoolean("success");
            } catch (JSONException e) {
                throw new ApiException(e.getMessage());
            }
        }

        public @NonNull JSONObject json() throws ApiException {
            try {
                JSONObject json = execute();
                if (json.has("success") && !json.getBoolean("success")) throw new ApiException(json);
                return json.getJSONObject("result");
            } catch (JSONException e) {
                throw new ApiException(e.getMessage());
            }
        }

        public @NonNull JSONArray array() throws ApiException {
            try {
                JSONObject json = execute();
                if (json.has("success") && !json.getBoolean("success")) throw new ApiException(json);
                return json.getJSONArray("result");
            } catch (JSONException e) {
                throw new ApiException(e.getMessage());
            }
        }

        private JSONObject execute() throws ApiException {
            RequestFuture<JSONObject> future = RequestFuture.newFuture();
            JsonObjectRequest request = new JsonObjectRequest(method, url, new JSONObject(data), future, future) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String>  headers = new HashMap<>();
                    if (!TextUtils.isEmpty(token))
                        headers.put(Constants.API_TOKEN_HEADER, token);
                    return headers;
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    if (volleyError.networkResponse != null && volleyError.networkResponse.statusCode == 500) {
                        return null;
                    }
                    return super.parseNetworkError(volleyError);
                }
            };
            request.setShouldCache(cache);
            if (!TextUtils.isEmpty(tag)) request.setTag(tag);
            getQueue().add(request);
            try {
                JSONObject json = future.get(10, TimeUnit.SECONDS);
                //Log.v(TAG, "json: " + json);
                /*if (json.getBoolean("success") && json.has("query"))
                    Log.v(TAG, "QUERY: " + json.getString("query"));*/
                return json;
            } catch (InterruptedException | TimeoutException | ExecutionException e) {
                Log.e(TAG, e.getMessage(), e);
                throw new ApiException(e.getMessage());
            }
        }
    }

    private Api(){}

    public boolean isValidToken(String token) {
        String url = Constants.API_URL + "verify";
        try {
            return new ApiRequest(token, url).success();
        } catch (ApiException e) {
            return false;
        }
    }

    public String authenticate(Account account) throws AuthenticatorException {
        AccountManager am = AccountManager.get(context);
        return authenticate(account.name, am.getPassword(account));
    }

    public String authenticate(String email, String pwd) throws AuthenticatorException {
        String url = Constants.API_URL + "signin";
        try {
            Map<String, String> map = new HashMap<>();
            map.put("email", email);
            map.put("password", pwd);
            JSONObject result = new ApiRequest(url, Request.Method.POST).data(map).json();
            return result.getString("token");
        } catch (ApiException e) {
            Log.e(TAG, e.getMessage(), e);
            throw new AuthenticatorException(e.getMessage());
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return null;
    }

    public Bundle me(String token) {
        Bundle data = null;
        String url = Constants.API_URL + "me";
        try {
            JSONObject result = new ApiRequest(token, url).json();
            data = new Bundle();
            data.putString("id", String.valueOf(result.getInt("id")));
            data.putString("email", result.getString("email"));
            data.putString("firstname", result.getString("firstname"));
            data.putString("lastname", result.getString("lastname"));
        } catch (ApiException | JSONException e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return data;
    }

    public <T extends AbstractEntity> List<T> load(Class<T> clzz, String token, Account account) {
        return load(clzz, token, account, 0);
    }
    public <T extends AbstractEntity> List<T> load(Class<T> clzz, String token, Account account, long lastSyncTime) {
        try {
            T inst = clzz.newInstance();
            String url = Constants.API_URL + inst.getApiPath() + "?last_sync=" + (lastSyncTime/1000);
            try {
                JSONArray remotes = new ApiRequest(token, url).array();
                List<T> list = new ArrayList<>();
                for (int i=0; i<remotes.length(); i++) {
                    try {
                        inst = clzz.newInstance();
                        inst.from(account, remotes.getJSONObject(i));
                        list.add(inst);
                    } catch (JSONException ignored){}
                }
                return list;
            } catch (ApiException e) {
                Log.e(TAG, e.getMessage(), e);
            }
        } catch (InstantiationException | IllegalAccessException e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return null;
    }

    public <T extends AbstractEntity> boolean save(AbstractEntity entity, String token, Account account) {
        String url = Constants.API_URL + entity.getApiPath();
        if (entity.getSourceId() > 0) url += "/" + entity.getSourceId();
        try {
            return new ApiRequest(token, url, entity.getSourceId() > 0 ? Request.Method.PUT : Request.Method.POST)
                    .data(entity.asJSON(account)).success();
        } catch (ApiException e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return false;
    }

    public boolean delete(AbstractEntity entity, String token) {
        String url = Constants.API_URL + entity.getApiPath() + "/" + entity.getSourceId();
        try {
            return new ApiRequest(token, url, Request.Method.DELETE).success();
        } catch (ApiException e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return false;
    }
}
