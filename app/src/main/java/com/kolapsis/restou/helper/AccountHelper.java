package com.kolapsis.restou.helper;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import com.kolapsis.restou.Constants;
import com.kolapsis.restou.api.Api;

import java.io.IOException;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

public class AccountHelper {

    public static final String TAG = Constants.APP_NAME + ".AccountHelper";

    public interface SyncStateChangeListener {
        void syncStateChanged(String authority, int state);
    }

    public static final int SYNC_FINISH = 0;
    public static final int SYNC_PROGRESS = 1;

    private static Context context;
    private static SharedPreferences preferences;
    private static AccountManager accountManager;
    private static Account account;
    private static Map<String, SyncStateChangeListener> syncListener = new LinkedHashMap<>();

    private AccountHelper() {}

    public static void setContext(Context ctx) {
        //Log.i(TAG, "-> setContext: " + ctx.getPackageName());
        context = ctx;
        Api.setContext(context);
        RestaurantHelper.setContext(context);
        VoteHelper.setContext(context);
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        accountManager = AccountManager.get(context);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        if (account == null && prefs.contains("selected_account")) {
            String name = prefs.getString("selected_account", null);
            if (name != null) account = getAccountByTypeAndName(Constants.ACCOUNT_TYPE, name);
        }
    }

    public static String getPassword(Account account) {
        return accountManager.getPassword(account);
    }

    public static String blockingGetAuthToken(Account account, String authTokenType, boolean manual) throws AuthenticatorException, IOException {
        //if (Constants.DEBUG) Log.d(TAG, "blockingGetAuthToken('"+account.name+"', '"+authTokenType+"')");
        if (context == null || accountManager == null) return null;
        String authToken = null;
        try {
            authToken = accountManager.blockingGetAuthToken(account, authTokenType, true);
            boolean valid = Api.getInstance().isValidToken(authToken);
            if (!valid || manual) {
                if (authToken != null) accountManager.invalidateAuthToken(authTokenType, authToken);
                accountManager.setAuthToken(account, authTokenType, null);
                authToken = accountManager.blockingGetAuthToken(account, authTokenType, true);
            }
        } catch (OperationCanceledException e) {
            Log.e(TAG, "OperationCanceledException" + e.getMessage());
        }
        //if (Constants.DEBUG) Log.d(TAG, "-> token: " + authToken);
        return authToken;
    }

    public static String getSyncKey(String authority) {
        return "last_sync_" + authority.substring(authority.lastIndexOf(".")+1).toLowerCase(Locale.getDefault());
    }
    /*public static void verifySyncTime(String authority) {
        String key = getSyncKey(authority);
        long time = preferences.getLong(key+"_time", 0);
        if ((Calendar.getInstance().getTimeInMillis() - time) > 3600 * 10) {
            time = Calendar.getInstance().getTimeInMillis();
            preferences.edit().putInt(key+"_state", SYNC_FINISH).putLong(key+"_time", time).commit();
        }
    }*/
    @SuppressLint("CommitPrefEdits")
    public static long updateSyncState(String authority, int state) {
        String key = getSyncKey(authority);
        long last = 0, time = Calendar.getInstance().getTimeInMillis();
        if (state == SYNC_PROGRESS) last = preferences.getLong(key+"_time", 0);
        preferences.edit().putInt(key+"_state", state).putLong(key+"_time", time).commit();
        if (syncListener.containsKey(authority) && syncListener.get(authority) != null) {
            syncListener.get(authority).syncStateChanged(authority, state);
        }
        return last;
    }
    public static void setSyncStateChangeListener(String authority, SyncStateChangeListener listener) {
        syncListener.put(authority, listener);
        if (listener != null && preferences.getInt(getSyncKey(authority) + "_state", SYNC_FINISH) == SYNC_PROGRESS) listener.syncStateChanged(authority, SYNC_PROGRESS);
    }

    public static Account[] getAccounts() {
        return accountManager.getAccountsByType(Constants.ACCOUNT_TYPE);
    }
    public static Account[] getAccountsByType(String type) {
        return accountManager.getAccountsByType(type);
    }
    public static Account getAccountByName(String name) {
        if (name == null) return null;
        Account[] accounts = accountManager.getAccounts();
        for (Account account : accounts)
            if ((Constants.ACCOUNT_TYPE.equals(account.type)) && name.equals(account.name))
                return account;
        return null;
    }
    public static Account getAccountByTypeAndName(String type, String name) {
        Account[] accounts = accountManager.getAccountsByType(type);
        for (Account account : accounts)
            if (name.equals(account.name)) return account;
        return null;
    }
    public static Account getAccountByUsername(String name) {
        if (name == null) return null;
        Account[] accounts = accountManager.getAccounts();
        for (Account account : accounts)
            if ((Constants.ACCOUNT_TYPE.equals(account.type)) && name.equals(account.name))
                return account;
        return null;
    }

    public static void setCurrentAccount(Account account) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        if (Constants.ACCOUNT_TYPE.equals(account.type) && !account.equals(AccountHelper.account)) {
            AccountHelper.account = account;
            prefs.edit().putString("selected_account", AccountHelper.account.name).commit();
        }
    }

    public static void setData(String name, String value) {
        setData(getAccount(), name, value);
    }
    public static void setData(Account account, String name, String value) {
        if (account == null) return;
        accountManager.setUserData(account, name, value);
        //Log.v(TAG, "setData: " + name + " = " + value);
    }
    public static String getData(String name) {
        return getData(getAccount(), name);
    }
    public static String getData(Account account, String name) {
        if (account == null) return null;
        return accountManager.getUserData(account, name);
    }

    public static boolean hasAccount() {
        return accountManager != null && accountManager.getAccountsByType(Constants.ACCOUNT_TYPE).length > 0;
    }
    public static Account getAccount() {
        if (account == null && hasAccount()) {
            Account[] accounts = accountManager.getAccountsByType(Constants.ACCOUNT_TYPE);
            account = accounts[0];
        }
        return account;
    }



}



