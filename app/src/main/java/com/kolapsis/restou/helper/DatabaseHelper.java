package com.kolapsis.restou.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.kolapsis.restou.Constants;
import com.kolapsis.restou.entity.Restaurant;
import com.kolapsis.restou.entity.Vote;

/**
 * Created by Benjamin Touchard on 18/04/17.
 * http://www.kolapsis.com
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String TAG = Constants.APP_NAME + ".DatabaseHelper";

    public static void invalidate(Context context) {
        Log.v(TAG, "===========================================");
        Log.d(TAG, "invalidate: " + Constants.BDD_NAME + " " + Constants.BDD_VERSION);
        DatabaseHelper sql = new DatabaseHelper(context);
        sql.onUpgrade(sql.getWritableDatabase(), Constants.BDD_VERSION, Constants.BDD_VERSION);
        Log.v(TAG, "===========================================");
    }

    public DatabaseHelper(Context context) {
        super(context, Constants.BDD_NAME, null, Constants.BDD_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Restaurant.Columns.CREATE_TABLE);
        db.execSQL(Vote.Columns.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(Restaurant.Columns.DROP_TABLE);
        db.execSQL(Vote.Columns.DROP_TABLE);
        onCreate(db);
    }
}
