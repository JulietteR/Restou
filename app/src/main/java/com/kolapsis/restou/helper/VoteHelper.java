package com.kolapsis.restou.helper;

import android.accounts.Account;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.content.CursorLoader;
import android.text.TextUtils;
import com.kolapsis.restou.Constants;
import com.kolapsis.restou.entity.AbstractEntity;
import com.kolapsis.restou.entity.Vote;
import com.kolapsis.restou.utils.StringUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by Benjamin Touchard on 19/04/17.
 * http://www.kolapsis.com
 */
@SuppressLint("StaticFieldLeak")
public class VoteHelper {

    private static final String TAG = Constants.APP_NAME + ".VoteHelper";

    private static Context sContext;
    private static ContentResolver sContentResolver;

    public static void setContext(Context ctx) {
        sContext = ctx;
        sContentResolver = sContext.getContentResolver();
    }

    public static class Loader extends CursorLoader {

        public Loader(Context context, Account account, Uri uri) {
            super(context);
            setUri(uri);
            setSelection(Vote.Columns.ACCOUNT + " = '" + account.name + "'");
        }
    }

    public static List<Vote> getMarkedAs(@NonNull Account account, @NonNull AbstractEntity.MarkedAs mark) {
        return select(account, mark.value());
    }

    public static long getIdFromSourceId(@NonNull Account account, int sourceId) {
        if (sourceId == 0) return 0;
        long localId = 0;
        String where = Vote.Columns.ACCOUNT + " = '" + account.name + "' AND " + Vote.Columns.SOURCE_ID + " = " + sourceId;
        //Log.v(TAG, "--> where: " + where);
        Cursor cursor = sContentResolver.query(Vote.CONTENT_URI, new String[]{Vote.Columns.ID}, where, null, null);
        if (cursor != null) {
            if (cursor.moveToNext()) localId = cursor.getLong(0);
            cursor.close();
        }
        //Log.v(TAG, "--> localId: " + localId);
        return localId;
    }

    public static int getSourceIdFromId(@NonNull Account account, long localId) {
        int sourceId = 0;
        String where = Vote.Columns.ACCOUNT + " = '" + account.name + "' AND " + Vote.Columns.ID + " = " + localId;
        Cursor cursor = sContentResolver.query(Vote.CONTENT_URI, new String[]{Vote.Columns.SOURCE_ID}, where, null, null);
        if (cursor != null) {
            if (cursor.moveToNext()) sourceId = cursor.getInt(0);
            cursor.close();
        }
        return sourceId;
    }

    public static Vote select(@NonNull Account account, long localId) {
        List<Vote> list = select(account, Vote.Columns.ID + " = " + localId);
        return list.size() > 0 ? list.get(0) : null;
    }

    public static Vote select(@NonNull Account account, int sourceId) {
        List<Vote> list = select(account, Vote.Columns.SOURCE_ID + " = " + sourceId);
        return list.size() > 0 ? list.get(0) : null;
    }

    public static List<Vote> select(@NonNull Account account) {
        return select(account, null);
    }

    private static List<Vote> select(@NonNull Account account, String where) {
        List<Vote> list = new ArrayList<>();
        String w = Vote.Columns.getWhere(account);
        if (!TextUtils.isEmpty(where)) w += " AND " + where;
        else w += " AND " + Vote.Columns.DELETE + " = 0";
        Cursor cursor = sContentResolver.query(Vote.CONTENT_URI, null, w, null, null);
        if (cursor != null)
            if (cursor.moveToFirst()) {
                do {
                    list.add(new Vote(cursor));
                }
                while (cursor.moveToNext());
                cursor.close();
            }
        return list;
    }

    public static int count(@NonNull Account account, long restaurantId) {

        String where = Vote.Columns.getWhere(account);
        where += " AND " + Vote.Columns.RESTAURANT_ID + " = " + restaurantId;
        return count(account, where);
    }

    public static int count(@NonNull Account account, int userId) {

        String where = Vote.Columns.getWhere(account);
        where += " AND " + Vote.Columns.USER_ID + " = " + userId;
        return count(account, where);
    }

    private static int count(@NonNull Account account, String where) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.FRANCE);
        Calendar c = Calendar.getInstance(Locale.FRANCE);
        where += " AND " + Vote.Columns.DATE + " = '" + sdf.format(c.getTime()) + "'";
        where += " AND " + Vote.Columns.DELETE + " = 0";
        // Log.v(TAG, w);
        Cursor cursor = sContentResolver.query(Vote.CONTENT_URI, null, where, null, null);
        if (cursor != null)
            return cursor.getCount();
        return 0;
    }

    public static Uri insert(Account account, Vote vote) {
        return insert(account, vote, false);
    }

    public static Uri insert(Account account, Vote vote, boolean callerIsSyncAdapter) {
        Uri uri = Vote.CONTENT_URI.buildUpon().appendQueryParameter(Constants.CALLER_IS_SYNCADAPTER, String.valueOf(callerIsSyncAdapter)).build();
        ContentValues values = vote.asContent(account);
        // Log.v(TAG, "INSERT " + values.toString());
        return sContentResolver.insert(uri, values);
    }

    public static int update(Account account, Vote restaurant) {
        return update(account, restaurant, false);
    }

    public static int update(Account account, Vote restaurant, boolean callerIsSyncAdapter) {
        Uri uri = restaurant.getUri().buildUpon().appendQueryParameter(Constants.CALLER_IS_SYNCADAPTER, String.valueOf(callerIsSyncAdapter)).build();
        ContentValues values = restaurant.asContent(account);
        values.put(Vote.Columns.DIRTY, !callerIsSyncAdapter);
        // Log.v(TAG, "UPDATE " + values.toString());
        return sContentResolver.update(uri, values, null, null);
    }

    public static int delete(Account account, Vote restaurant) {
        return delete(account, restaurant, false);
    }

    public static int delete(Account account, Vote restaurant, boolean callerIsSyncAdapter) {
        Uri uri = restaurant.getUri();
        if (callerIsSyncAdapter)
            uri = uri.buildUpon().appendQueryParameter(Constants.CALLER_IS_SYNCADAPTER, "true").build();
        int count;
        if (callerIsSyncAdapter) count = sContentResolver.delete(uri, null, null);
        else {
            ContentValues values = new ContentValues();
            values.put(Vote.Columns.DELETE, true);
            count = sContentResolver.update(uri, values, null, null);
        }
        // Log.i(TAG, "--> deleted " + uri + " > " + count);
        return count;
    }

    public static int delete(Account account, List<Integer> ids) {
        String where = Vote.Columns.getWhere(account) + " AND " + Vote.Columns.SOURCE_ID + " NOT IN (?)";
        return sContentResolver.delete(Vote.CONTENT_URI, where, new String[]{StringUtils.join(ids, ",")});
    }

}
