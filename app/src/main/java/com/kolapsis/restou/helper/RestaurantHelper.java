package com.kolapsis.restou.helper;

import android.accounts.Account;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.content.CursorLoader;
import android.text.TextUtils;
import com.kolapsis.restou.Constants;
import com.kolapsis.restou.entity.AbstractEntity;
import com.kolapsis.restou.entity.Restaurant;
import com.kolapsis.restou.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Benjamin Touchard on 19/04/17.
 * http://www.kolapsis.com
 */
@SuppressLint("StaticFieldLeak")
public class RestaurantHelper {

    private static final String TAG							= Constants.APP_NAME + ".RestaurantHelper";

    private static Context sContext;
    private static ContentResolver sContentResolver;

    public static void setContext(Context ctx) {
        sContext = ctx;
        sContentResolver = sContext.getContentResolver();
    }

    public static class Loader extends CursorLoader {

        public Loader(Context context, Account account, Uri uri) {
            super(context);
            setUri(uri);
            setSelection(Restaurant.Columns.ACCOUNT + " = '" + account.name + "'");
        }
    }

    public static List<Restaurant> getMarkedAs(@NonNull Account account, @NonNull AbstractEntity.MarkedAs mark) {
        return select(account, mark.value());
    }

    public static long getIdFromSourceId(@NonNull Account account, int sourceId) {
        if (sourceId == 0) return 0;
        long localId = 0;
        String where = Restaurant.Columns.ACCOUNT + " = '" + account.name + "' AND " + Restaurant.Columns.SOURCE_ID + " = " + sourceId;
        //Log.v(TAG, "--> where: " + where);
        Cursor cursor = sContentResolver.query(Restaurant.CONTENT_URI, new String[] { Restaurant.Columns.ID }, where, null, null);
        if (cursor != null) {
            if (cursor.moveToNext()) localId = cursor.getLong(0);
            cursor.close();
        }
        //Log.v(TAG, "--> localId: " + localId);
        return localId;
    }
    public static int getSourceIdFromId(@NonNull Account account, long localId) {
        int sourceId = 0;
        String where = Restaurant.Columns.ACCOUNT + " = '" + account.name + "' AND " + Restaurant.Columns.ID + " = " + localId;
        Cursor cursor = sContentResolver.query(Restaurant.CONTENT_URI, new String[] { Restaurant.Columns.SOURCE_ID }, where, null, null);
        if (cursor != null) {
            if (cursor.moveToNext()) sourceId = cursor.getInt(0);
            cursor.close();
        }
        return sourceId;
    }

    public static Restaurant select(@NonNull Account account, long localId) {
        List<Restaurant> list = select(account, Restaurant.Columns.ID + " = " + localId);
        return list.size() > 0 ? list.get(0) : null;
    }
    public static Restaurant select(@NonNull Account account, int sourceId) {
        List<Restaurant> list = select(account, Restaurant.Columns.SOURCE_ID + " = " + sourceId);
        return list.size() > 0 ? list.get(0) : null;
    }
    public static List<Restaurant> select(@NonNull Account account) {
        return select(account, null);
    }
    private static List<Restaurant> select(@NonNull Account account, String where) {
        List<Restaurant> list = new ArrayList<>();
        String w = Restaurant.Columns.getWhere(account);
        if (!TextUtils.isEmpty(where)) w += " AND " + where;
        else w += " AND " + Restaurant.Columns.DELETE + " = 0";
        Cursor cursor = sContentResolver.query(Restaurant.CONTENT_URI, null, w , null, null);
        if (cursor != null)
            if (cursor.moveToFirst()) {
                do { list.add(new Restaurant(cursor)); }
                while (cursor.moveToNext());
                cursor.close();
            }
        return list;
    }

    public static Uri insert(Account account, Restaurant restaurant) {
        return insert(account, restaurant, false);
    }
    public static Uri insert(Account account, Restaurant restaurant, boolean callerIsSyncAdapter) {
        Uri uri = Restaurant.CONTENT_URI.buildUpon().appendQueryParameter(Constants.CALLER_IS_SYNCADAPTER, String.valueOf(callerIsSyncAdapter)).build();
        ContentValues values = restaurant.asContent(account);
        return sContentResolver.insert(uri, values);
    }

    public static int update(Account account, Restaurant restaurant) {
        return update(account, restaurant, false);
    }
    public static int update(Account account, Restaurant restaurant, boolean callerIsSyncAdapter) {
        Uri uri = restaurant.getUri().buildUpon().appendQueryParameter(Constants.CALLER_IS_SYNCADAPTER, String.valueOf(callerIsSyncAdapter)).build();
        ContentValues values = restaurant.asContent(account);
        values.put(Restaurant.Columns.DIRTY, !callerIsSyncAdapter);
        return sContentResolver.update(uri, values, null, null);
    }

    public static int delete(Account account, Restaurant restaurant) {
        return delete(account, restaurant, false);
    }
    public static int delete(Account account, Restaurant restaurant, boolean callerIsSyncAdapter) {
        Uri uri = restaurant.getUri();
        if (callerIsSyncAdapter)
            uri = uri.buildUpon().appendQueryParameter(Constants.CALLER_IS_SYNCADAPTER, "true").build();
        int count;
        if (callerIsSyncAdapter) count = sContentResolver.delete(uri, null, null);
        else {
            ContentValues values = new ContentValues();
            values.put(Restaurant.Columns.DELETE, true);
            count = sContentResolver.update(uri, values, null, null);
        }
        // Log.i(TAG, "--> deleted " + uri + " > " + count);
        return count;
    }
    public static int delete(Account account, List<Integer> ids) {
        String where = Restaurant.Columns.getWhere(account) + " AND " + Restaurant.Columns.SOURCE_ID + " NOT IN (?)";
        return sContentResolver.delete(Restaurant.CONTENT_URI, where, new String[]{StringUtils.join(ids, ",")});
    }

}
