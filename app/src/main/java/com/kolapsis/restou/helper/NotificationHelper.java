package com.kolapsis.restou.helper;

import android.accounts.Account;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.NotificationCompat;
import com.kolapsis.restou.Constants;
import com.kolapsis.restou.R;
import com.kolapsis.restou.api.Api;
import com.kolapsis.restou.entity.Restaurant;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Juliette RIVIERE on 27/06/2017.
 */
public class NotificationHelper {

    public static void triggerNotification(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
//        boolean active = preferences.getBoolean("notifications", true);*

        int id = Integer.parseInt(AccountHelper.getData("id"));
        int userHasVoted = VoteHelper.count(AccountHelper.getAccount(), id);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, Constants.NOTIFICATION_DONE, new Intent(Constants.NOTIFICATION_DONE_ACTION), 0);
        Bundle args = new Bundle();
        args.putBoolean("close_notification", true);
        int cntRestaurantTop = 0;
        int idRestaurantTop = 0;
//        userHasVoted = 1;
        // On test si l'utilisateur a bien voté
        if (userHasVoted > 0) {

            //on itère sur la liste des restaurants et on récupère celui avec le meilleur score
            for (int idRestaurant = 0; idRestaurant < 100; idRestaurant++) {
                  Integer idSourceRestaurant = VoteHelper.getSourceIdFromId(AccountHelper.getAccount(), idRestaurant);

                Integer cntRestaurant = VoteHelper.count(AccountHelper.getAccount(), idSourceRestaurant);
                if (idRestaurant != 0) {
                    if (cntRestaurant > cntRestaurantTop) {
                        cntRestaurantTop = VoteHelper.count(AccountHelper.getAccount(), idSourceRestaurant);
                        idRestaurantTop = idSourceRestaurant;
                    }
                } else {
                    cntRestaurantTop = VoteHelper.count(AccountHelper.getAccount(), idSourceRestaurant);
                    idRestaurantTop = idSourceRestaurant;
                }
            }

            // on affiche le nom du restaurant gagnant
            Restaurant restaurantTop = RestaurantHelper.select(AccountHelper.getAccount(), idRestaurantTop);
            String restaurantName = restaurantTop.getName();
            Notification notification = new NotificationCompat.Builder(context)
                    .setAutoCancel(false)
                    .setTicker("Le restaurant choisi est : " + restaurantName)
                    .setContentTitle("Le restaurant choisi est : " + restaurantName)
                    .setContentText("Le restaurant choisi est : " + restaurantName)
                    .setSmallIcon(R.drawable.ic_restaurant)
                    .addAction(R.drawable.ic_restaurant, "Fermer", pendingIntent)
                    .build();
            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            manager.notify(Constants.NOTIFICATION_CHOICE_ID, notification);
        } else {
            Notification notification = new NotificationCompat.Builder(context)
                    .setAutoCancel(false)
                    .setTicker("Vous devez voter pour un restaurant")
                    .setContentTitle("Vous devez voter pour un restaurant")
                    .setContentText("Vous devez voter pour un restaurant")
                    .setSmallIcon(R.drawable.ic_restaurant)
                    .addAction(R.drawable.ic_restaurant, "Fermer", pendingIntent)
                    .build();
            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            manager.notify(Constants.NOTIFICATION_CHOICE_ID, notification);
        }
    }

    public static void showChoice(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        boolean active = preferences.getBoolean("notifications", true);
        if (active) {

            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, Constants.NOTIFICATION_DONE, new Intent(Constants.NOTIFICATION_DONE_ACTION), 0);
            Bundle args = new Bundle();
            Intent intent = new Intent();
            args.putBoolean("close_notification", true);
            PendingIntent pendingOpen = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT, args);

            Notification notification = new NotificationCompat.Builder(context)
                    .setAutoCancel(false)
                    .setTicker("Le restaurant est choisi")
                    .setContentTitle("Le restaurant est choisi")
                    .setContentText("Le restaurant est choisi")
                    .setSmallIcon(R.drawable.ic_restaurant)
//                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_restaurant))
                    .addAction(R.drawable.ic_restaurant, "Fermer", pendingIntent)
                    .build();
            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            manager.notify(Constants.NOTIFICATION_CHOICE_ID, notification);
        }
    }

    public static void close(Context context) {
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.cancel(Constants.NOTIFICATION_CHOICE_ID);
    }

    private NotificationHelper() {
    }


}
