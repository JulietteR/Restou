package com.kolapsis.restou.utils;

import java.util.List;

/**
 * Created by Benjamin Touchard on 19/04/17.
 * http://www.kolapsis.com
 */
public class StringUtils {

    public static boolean isJSON(String str) {
        String first = str.substring(0, 1);
        String last = str.substring(str.length()-1);
        return ("{".equals(first) && "}".equals(last)) || "[".equals(first) && "]".equals(last);
    }

    public static String join(List<?> list, String separator) {
        if (list == null) return "";
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (Object o : list) {
            if (first) first = false;
            else sb.append(separator);
            sb.append(String.valueOf(o));
        }
        return sb.toString();
    }
}
