package com.kolapsis.restou.authenticator;

import android.accounts.*;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import com.kolapsis.restou.Constants;
import com.kolapsis.restou.LoginActivity;
import com.kolapsis.restou.api.Api;
import com.kolapsis.restou.helper.AccountHelper;

public class Authenticator extends AbstractAccountAuthenticator {

    public static final String TAG 							= Constants.APP_NAME + ".Authenticator";

    public static final String PARAM_CONFIRM_CREDENTIALS 	= "confirmCredentials";
    public static final String PARAM_ACCOUNT_NAME		 	= "accountName";
    public static final String PARAM_AUTHTOKEN_TYPE 		= "authtokenType";

    private final Context mContext;

    public Authenticator(Context context) {
        super(context);
        //Log.i(TAG, "onCreate();");
        mContext = context;
        AccountHelper.setContext(context);
    }

    @Override
    public Bundle addAccount(AccountAuthenticatorResponse response, String accountType, String authTokenType, String[] requiredFeatures, Bundle options) {
        Log.v(TAG, "addAccount("+accountType+")");
        Intent intent = getAuthIntent();
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
        final Bundle bundle = new Bundle();
        bundle.putParcelable(AccountManager.KEY_INTENT, intent);
        return bundle;
    }

    @Override
    public Bundle confirmCredentials(AccountAuthenticatorResponse response, Account account, Bundle options) {
        Log.v(TAG, "confirmCredentials()");
        // throw new UnsupportedOperationException();
        AccountManager am = AccountManager.get(mContext);
        Intent intent = getAuthIntent();
        intent.putExtra(PARAM_ACCOUNT_NAME, account.name);
        intent.putExtra(PARAM_CONFIRM_CREDENTIALS, true);
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
        Bundle bundle = new Bundle();
        bundle.putParcelable(AccountManager.KEY_INTENT, intent);
        return bundle;
    }

    @Override
    public Bundle editProperties(AccountAuthenticatorResponse response, String accountType) {
        Log.v(TAG, "editProperties()");
        throw new UnsupportedOperationException();
    }

    @Override
    public Bundle getAuthToken(AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle loginOptions) throws NetworkErrorException {
        Log.d(TAG, "-> getAuthToken("+authTokenType+")");
        if (!isValidTokenType(authTokenType)) {
            final Bundle result = new Bundle();
            result.putString(AccountManager.KEY_ERROR_MESSAGE, "invalid authTokenType");
            return result;
        }

        Intent intent;
        if (!TextUtils.isEmpty(AccountHelper.getPassword(account))) {
            String authToken = null;
            try {
                authToken = Api.getInstance().authenticate(account);
            } catch (AuthenticatorException e) {
                Log.v(TAG, "AuthenticatorException: " + e.getMessage());
            }
            Log.i(TAG, "--> authToken: " + authToken);

            if (!TextUtils.isEmpty(authToken)) {
                final Bundle result = new Bundle();
                result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name);
                result.putString(AccountManager.KEY_ACCOUNT_TYPE, Constants.ACCOUNT_TYPE);
                result.putString(AccountManager.KEY_AUTHTOKEN, authToken);
                return result;
            } else {
                AccountManager am = AccountManager.get(mContext);
                intent = getAuthIntent();
                intent.putExtra(PARAM_ACCOUNT_NAME, account.name);
                intent.putExtra(PARAM_AUTHTOKEN_TYPE, authTokenType);
                intent.putExtra(PARAM_CONFIRM_CREDENTIALS, true);
            }
        } else {
            intent = getAuthIntent();
            intent.putExtra(PARAM_ACCOUNT_NAME, account.name);
            intent.putExtra(PARAM_AUTHTOKEN_TYPE, authTokenType);
            intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
        }

        Bundle bundle = new Bundle();
        bundle.putParcelable(AccountManager.KEY_INTENT, intent);
        return bundle;
    }

    @Override
    public String getAuthTokenLabel(String authTokenType) {
        return Constants.APP_NAME;
    }

    @Override
    public Bundle hasFeatures(AccountAuthenticatorResponse response, Account account, String[] features) {
        // This call is used to query whether the Authenticator supports
        // specific features. We don't expect to get called, so we always
        // return false (no) for any queries.
        Log.v(TAG, "hasFeatures()");
        final Bundle result = new Bundle();
        result.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, false);
        return result;
    }

    @Override
    public Bundle updateCredentials(AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle loginOptions) {
        Log.v(TAG, "updateCredentials()");
        throw new UnsupportedOperationException();
    }
    private boolean isValidTokenType(String authTokenType) {
        return authTokenType.equals(Constants.AUTH_TOKEN_TYPE);
    }
    private Intent getAuthIntent() {
        return new Intent(mContext, LoginActivity.class);
    }
}
